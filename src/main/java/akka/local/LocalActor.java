package akka.local;

import akka.actor.*;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;

/**
 * Created by user on 09.07.17.
 */
public class LocalActor extends UntypedActor {


    @Override
    public void preStart() {
        /*
          Connect to remote actor. The following are the different parts of actor path

          akka.tcp : enabled-transports  of remote_application.conf

          RemoteSystem : name of the actor system used to create remote actor

          127.0.0.1:5150 : host and port

          user : The actor is user defined

          remote : name of the actor, passed as parameter to system.actorOf call

         */
        ActorSelection remoteActor = context().actorSelection("akka.tcp://RemoteSystem@127.0.0.1:5150/user/remote");
        System.out.println("That 's remote:" + remoteActor);
        remoteActor.tell("hi", self());
    }

    @Override
    public void onReceive(Object message) throws Exception {
        System.out.println("Got message from remote: " + message);
    }

    public static void main(String... args) {
        String configFile = LocalActor.class.getClassLoader().getResource("local_application.conf").getFile();
        Config config = ConfigFactory.parseFile(new File(configFile));
        ActorSystem system = ActorSystem.apply("ClientSystem",config);
        ActorRef localActor = system.actorOf(Props.create(LocalActor.class), "local");
    }
}
