package akka.remote;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;

/**
 * Created by user on 09.07.17.
 */
public class RemoteActor extends UntypedActor {
    @Override
    public void onReceive(Object message) throws Exception {
        System.out.println("Remote received: " + message + " from sender " + sender());
        sender().tell("hi", self());
    }

    public static void main(String... args) {
        //get the configuration file from classpath
        String configFile = RemoteActor.class.getClassLoader().getResource("remote_application.conf").getFile();
        //parse the config
        Config config = ConfigFactory.parseFile(new File(configFile));
        //create an actor system with that config
        ActorSystem system = ActorSystem.apply("RemoteSystem", config);
        //create a remote actor from actorSystem
        ActorRef remote = system.actorOf(Props.create(RemoteActor.class), "remote");
        System.out.println("remote is ready");
    }
}
